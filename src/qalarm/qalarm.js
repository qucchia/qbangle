// This file shows the alarm

let L = require("qlocale").getAppLocale("qalarm");

function getCurrentTime() {
  let date = new Date();
  return (
    date.getHours() * 3600000 +
    date.getMinutes() * 60000 +
    date.getSeconds() * 1000
  );
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getRandomFromRange(
  lowerRangeMin,
  lowerRangeMax,
  higherRangeMin,
  higherRangeMax
) {
  let lowerRange = lowerRangeMax - lowerRangeMin;
  let higherRange = higherRangeMax - higherRangeMin;
  let fullRange = lowerRange + higherRange;
  let randomNum = getRandomInt(fullRange);
  if (randomNum <= lowerRangeMax - lowerRangeMin) {
    return randomNum + lowerRangeMin;
  } else {
    return randomNum + (higherRangeMin - lowerRangeMax);
  }
}

function showNumberPicker(currentGuess, randomNum) {
  E.showMessage(
    currentGuess +
      // Show "PRESS BTN 2" when on the correct number
      (currentGuess === randomNum ? "\n" + L.pressBtn2.toUpperCase() : ""),
    L.getTo + " " + randomNum
  );
}

function showPrompt(message, buzzCount, alarm) {
  let prompt = { title: (alarm.timer ? L.timer : L.alarm).toUpperCase() + "!" };
  prompt.buttons = {};
  prompt.buttons[L.sleep] = true;
  prompt.buttons[L.ok] = false;

  E.showPrompt(message, prompt).then(function (sleep) {
    buzzCount = 0;
    if (sleep) {
      // Sleep 10 minutes
      if (alarm.originalTime === undefined) alarm.originalTime = alarm.time;
      alarm.time += 10 / 60;
    } else {
      alarm.last = new Date().getDate();

      if (alarm.originalTime !== undefined) {
        alarm.time = alarm.originalTime;
        delete alarm.originalTime;
      }

      // Turn alarm off if repeat is isabled
      if (!alarm.repeat) alarm.on = false;
    }

    // Save alarm and load default clock
    require("Storage").write("qalarm.json", JSON.stringify(alarms));
    load();
  });
}

function showAlarm(alarm) {
  // Exit if quiet mode is set to Silent
  if ((require("Storage").readJSON("setting.json", 1) || {}).quiet > 1) return;

  let message = require("locale").time(new Date(alarm.time));
  let buzzCount = 20;
  if (alarm.message) message += "\n" + alarm.message + "!";

  if (alarm.hard) {
    let okClicked = false;
    let currentGuess = 10;
    let randomNum = getRandomFromRange(0, 7, 13, 20);
    showNumberPicker(currentGuess, randomNum);
    setWatch(
      (o) => {
        if (!okClicked && currentGuess < 20) {
          currentGuess = currentGuess + 1;
          showNumberPicker(currentGuess, randomNum);
        }
      },
      BTN1,
      { repeat: true, edge: "rising" }
    );

    setWatch(
      (o) => {
        if (currentGuess == randomNum) {
          okClicked = true;
          showPrompt(message, buzzCount, alarm);
        }
      },
      BTN2,
      { repeat: true, edge: "rising" }
    );

    setWatch(
      (o) => {
        if (!okClicked && currentGuess > 0) {
          currentGuess = currentGuess - 1;
          showNumberPicker(currentGuess, randomNum);
        }
      },
      BTN3,
      { repeat: true, edge: "rising" }
    );
  } else {
    showPrompt(message, buzzCount, alarm);
  }

  function buzz() {
    Bangle.buzz(500).then(() => {
      setTimeout(() => {
        Bangle.buzz(500).then(function () {
          setTimeout(() => {
            Bangle.buzz(2000).then(function () {
              if (buzzCount--) setTimeout(buzz, 2000);
              else if (alarm.autoSnooze) {
                buzzCount = 20;
                setTimeout(buzz, 600000); // 10 minutes
              }
            });
          }, 100);
        });
      }, 100);
    });
  }
  buzz();
}

let date = new Date();
let time = getCurrentTime();
let alarms = require("Storage").readJSON("qalarm.json", 1) || [];

let active = alarms.filter(
  (alarm) =>
    alarm.on &&
    alarm.time <= time &&
    alarm.last != date.getDate() &&
    (alarm.timer || alarm.daysOfWeek[date.getDay()])
);

if (active.length) {
  showAlarm(active.sort((a, b) => a.time - b.time)[0]);
}
