/* Qalarm is an alarm and timer app using the Qmenu framework. */

const Qmenu = require("qmenu");

exports = class Qalarm extends Qmenu {
  /**
   * Creates a Qalarm object.
   * @constructor
   * @param {string|Object[]} [alarms] - The array of alarms or the name of the file to load the alarms from.
   */
  constructor(alarms = "qalarm.json") {
    let locale = require("qlocale").getAppLocale("qalarm");
    super(locale, MENUS);

    if (typeof alarms === "string") {
      this.state.alarms = require("Storage").readJSON(alarms, 1) || [];
    } else {
      this.state.alarms = alarms;
    }
  }

  // TODO: Format according to Locale
  /**
   * Formats time in form HH:MM.
   * @param {number} time - Time in milliseconds since midnight.
   */
  formatTime(time) {
    let hours = 0 | (time / 3600000);
    let minutes = 0 | (time / 60000) % 60;
    return ("0" + hours).substr(-2) + ":" + ("0" + minutes).substr(-2);
  }

  /**
   * Formats timer period in form H:MM:SS.
   * @param {number} time - Timer period in seconds.
   */
  formatTimer(time) {
    let hours = 0 | (time / 3600);
    let minutes = 0 | (time / 60) % 60;
    let seconds = time % 3600;
    return (
      hours +
      ":" +
      ("0" + minutes).substr(-2) +
      ":" +
      ("0" + seconds).substr(-2)
    );
  }

  /** Gets time in milliseconds since midnight. */
  getCurrentTime() {
    let date = new Date();
    return (
      (date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds()) *
      1000
    );
  }

  /**
   * Gives Qmenu data for the "editAlarm" and "editTimer" menus.
   * @param {string} menuId - The ID of the menu.
   * @param {Object} parameters - The paramaters given.
   */
  getMenuData(menuId, parameters) {
    if (menuId === "editAlarm") {
      if (parameters.index === -1) {
        return {
          type: "alarm",
          hours: 12,
          minutes: 0,
          on: true,
          repeat: true,
          autoSnooze: false,
          hard: false,
          daysOfWeek: new Array(7).fill(true),
        };
      } else {
        return Object.assign({}, alarms[alarmIndex]);
      }
    } else if (menuId === "editTimer") {
      if (parameters.index === -1) {
        return {
          type: "timer",
          hours: 0,
          minutes: 5,
          seconds: 0,
          on: true,
          repeat: false,
          autoSnooze: false,
          hard: false,
        };
      } else {
        return Object.assign({}, alarms[alarmIndex]);
      }
    }
  }

  /**
   * Saves alarm data.
   * @param {string} menuId - The ID of the menu.
   * @param {Object} parameters - The parameters given.
   * @param {Object} alarm - The alarm to save.
   */
  setMenuData(menuId, parameters, alarm) {
    if (menuId === "editAlarm" || menuId === "editTimer") {
      if (menuId === "editAlarm") {
        alarm.time = (hours * 3600 + mins * 60) * 1000;
      } else {
        alarm.time = (getCurrentTime() + alarm.timer * 1000) % 86400000;
        alarm.on = true;
      }

      alarm.last = 0;
      if (alarm.time < getCurrentTime()) alarm.last = new Date().getDate();

      if (parameters.index === -1) alarms.push(alarm);
      else alarms[parameters.index] = alarm;

      require("Storage").write("qalarm.json", JSON.stringify(alarms));
      eval(require("Storage").read("qalarmcheck.js"));
    }
  }
};

let MENUS = {
  "": {
    title: "alarms",
    type: "UpDown",
    items: [
      {
        title: "createAlarm",
        type: "menu",
        id: "editAlarm?index=-1",
      },
      {
        title: "createTimer",
        type: "menu",
        id: "editTimer?index=-1",
      },
      {
        title: "editAlarms",
        type: "menu",
        id: "alarms",
      },
      {
        title: "editTimers",
        type: "menu",
        id: "timers",
      },
    ],
  },
  editAlarms: "dynamic",
  editTimers: "dynamic",
  editAlarm: {
    title: "alarm",
    type: "UpDown",
    items: [
      {
        title: "hours",
        type: "number",
        min: 0,
        max: 23,
        wrap: true,
        id: "hours",
      },
      {
        title: "minutes",
        type: "number",
        min: 0,
        max: 59,
        wrap: true,
        id: "minutes",
      },
      {
        title: "enabled",
        type: "yesNo",
        id: "on",
      },
      {
        title: "repeat",
        type: "yesNo",
        id: "repeat",
      },
      {
        title: "autoSnooze",
        type: "yesNo",
        id: "autoSnooze",
      },
      {
        title: "alarmHard",
        type: "yesNo",
        id: "hard",
      },
      {
        type: "save",
      },
      {
        title: "delete",
        type: "action",
        action: "delete",
      },
    ],
  },
};
