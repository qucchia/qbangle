Bangle.loadWidgets();
Bangle.drawWidgets();

let alarms = require("Storage").readJSON("qalarm.json", 1) || [];

/*
Alarm format:
{
  on : true,
  time : 23400000, // Time of day since midnight in ms
  message : "Eat chocolate", // (optional) Must be set manually from the IDE
  last : 0, // Last day of the month we alarmed on - so we don't alarm twice in one day!
  repeat : true, // Repeat
  autoSnooze : false, // Auto snooze
  hard: true, // Whether the alarm will be like HardAlarm or not
  timer : 300, // (optional) If set, this is a timer and it's the time in seconds
  daysOfWeek: [true,true,true,true,true,true,true] // What days of the week the alarm is on. First item is Sunday, 2nd is Monday, etc.
}
*/

let L = require("qlocale").getAppLocale("qalarm");
console.log(L);

function formatTime(time) {
  let hours = 0 | (time / 3600000);
  let minutes = 0 | (time / 60000) % 60;
  return ("0" + hours).substr(-2) + ":" + ("0" + minutes).substr(-2);
}

function formatTimer(time) {
  let hours = 0 | (time / 3600);
  let minutes = 0 | (time / 60) % 60;
  let seconds = time % 3600;
  return (
    hours + ":" + ("0" + minutes).substr(-2) + ":" + ("0" + seconds).substr(-2)
  );
}

function getCurrentTime() {
  let time = new Date();
  return (
    time.getHours() * 3600000 +
    time.getMinutes() * 60000 +
    time.getSeconds() * 1000
  );
}

function showMainMenu() {
  const menu = {
    "": { title: L.alarms },
  };
  menu["< " + L.back] = () => load();
  menu[L.createAlarm] = () => showEditAlarmMenu(-1);
  menu[L.createTimer] = () => showEditTimerMenu(-1);
  menu[L.editAlarms] = () => showAlarmsMenu();
  menu[L.editTimers] = () => showTimersMenu();

  if (WIDGETS["qalarm"]) WIDGETS["qalarm"].reload();
  return E.showMenu(menu);
}

function showAlarmsMenu() {
  const menu = { "": { title: L.alarms } };
  menu["< " + L.back] = () => showMainMenu();

  alarms
    .filter((a) => !a.timer)
    .forEach((alarm, idx) => {
      let text = (alarm.on ? L.on : L.off) + " " + formatTime(alarm.time);
      menu[text] = function () {
        showEditAlarmMenu(idx);
      };
    });

  E.showMenu(menu);
}

function showTimersMenu() {
  const menu = { "": { title: L.timers } };
  menu["< " + L.back] = () => showMainMenu();

  alarms
    .filter((a) => a.timer)
    .forEach((alarm, idx) => {
      let text = (alarm.on ? L.on : L.off) + " " + formatTimer(alarm.timer);
      menu[text] = function () {
        showEditTimerMenu(idx);
      };
    });

  E.showMenu(menu);
}

function showEditAlarmMenu(alarmIndex, alarm) {
  const newAlarm = alarmIndex < 0;

  if (!alarm) {
    if (newAlarm) {
      alarm = {
        time: 43200000,
        on: true,
        repeat: true,
        autoSnooze: false,
        hard: false,
        daysOfWeek: new Array(7).fill(true),
      };
    } else {
      alarm = Object.assign({}, alarms[alarmIndex]); // Copy object in case we don't save it
    }
  }

  let hrs = 0 | (alarm.time / 3600000);
  let mins = 0 | (alarm.time / 60000) % 60;

  const menu = {
    "": { title: alarm.msg ? alarm.msg : L.alarms },
  };
  menu["< " + L.back] = showMainMenu;
  menu[L.hours] = {
    value: hrs,
    onchange: function (v) {
      if (v < 0) v = 23;
      if (v > 23) v = 0;
      hrs = v;
      this.value = v;
    }, // no arrow fn -> preserve 'this'
  };
  menu[L.minutes] = {
    value: mins,
    onchange: function (v) {
      if (v < 0) v = 59;
      if (v > 59) v = 0;
      mins = v;
      this.value = v;
    }, // no arrow fn -> preserve 'this'
  };
  menu[L.enabled] = {
    value: alarm.on,
    format: (v) => (v ? L.on : L.off),
    onchange: (v) => (alarm.on = v),
  };
  menu[L.repeat] = {
    value: alarm.repeat,
    format: (v) => (v ? L.yes : L.no),
    onchange: (v) => (alarm.repeat = v),
  };
  menu[L.autoSnooze] = {
    value: alarm.autoSnooze,
    format: (v) => (v ? L.yes : L.no),
    onchange: (v) => (alarm.autoSnooze = v),
  };
  menu[L.alarmHard] = {
    value: alarm.hard,
    format: (v) => (v ? L.yes : L.no),
    onchange: (v) => (alarm.hard = v),
  };
  menu[L.daysOfWeek] = () => showDaysMenu(alarmIndex, getAlarm());

  function getAlarm() {
    alarm.time = hrs * 3600000 + mins * 60000;
    alarm.last = 0;
    // If alarm is for tomorrow not today (eg, in the past), set day
    if (alarm.time < getCurrentTime()) alarm.last = new Date().getDate();

    return alarm;
  }

  menu["> " + L.save] = function () {
    if (newAlarm) alarms.push(getAlarm());
    else alarms[alarmIndex] = getAlarm();
    require("Storage").write("qalarm.json", JSON.stringify(alarms));
    eval(require("Storage").read("qalarmcheck.js"));
    showMainMenu();
  };

  if (!newAlarm) {
    menu["> " + L.delete] = function () {
      alarms.splice(alarmIndex, 1);
      require("Storage").write("qalarm.json", JSON.stringify(alarms));
      eval(require("Storage").read("qalarmcheck.js"));
      showMainMenu();
    };
  }
  return E.showMenu(menu);
}

function showDaysMenu(alarmIndex, alarm) {
  const menu = {
    "": { title: alarm.msg ? alarm.msg : L.alarms },
  };

  menu["< " + L.back] = () => showEditAlarmMenu(alarmIndex, alarm);

  for (let i = 0; i < 7; i++) {
    let dayOfWeek = require("qlocale").day[i];
    menu[dayOfWeek] = {
      value: alarm.daysOfWeek[i],
      format: (v) => (v ? L.yes : L.no),
      onchange: (v) => (alarm.daysOfWeek[i] = v),
    };
  }

  return E.showMenu(menu);
}

function showEditTimerMenu(timerIndex) {
  var newAlarm = timerIndex < 0;

  let alarm;
  if (newAlarm) {
    alarm = {
      timer: 300,
      on: true,
      repeat: false,
      autoSnooze: false,
      hard: false,
    };
  } else {
    alarm = alarms[timerIndex];
  }

  let hrs = 0 | (alarm.timer / 3600);
  let mins = 0 | (alarm.timer / 60) % 60;
  let secs = (0 | alarm.timer) % 60;

  const menu = { "": { title: L.timer } };
  menu["< " + L.back] = showMainMenu;
  menu[L.hours] = {
    value: hrs,
    onchange: function (v) {
      if (v < 0) v = 23;
      if (v > 23) v = 0;
      hrs = v;
      this.value = v;
    }, // no arrow fn -> preserve 'this'
  };
  menu[L.minutes] = {
    value: mins,
    onchange: function (v) {
      if (v < 0) v = 59;
      if (v > 59) v = 0;
      mins = v;
      this.value = v;
    }, // no arrow fn -> preserve 'this'
  };
  menu[L.seconds] = {
    value: secs,
    onchange: function (v) {
      if (v < 0) v = 59;
      if (v > 59) v = 0;
      secs = v;
      this.value = v;
    }, // no arrow fn -> preserve 'this'
  };
  menu[L.alarmHard] = {
    value: alarm.hard,
    format: (v) => (v ? L.on : L.off),
    onchange: (v) => (alarm.hard = v),
  };
  menu["> " + L.start] = function () {
    alarm.timer = hrs * 3600 + mins * 60 + secs;
    alarm.time = (getCurrentTime() + alarm.timer * 1000) % 86400000;
    alarm.last = 0; // This allows the same timer to be used more than once in one day
    alarm.on = true;

    if (newAlarm) alarms.push(alarm);
    else alarms[timerIndex] = alarm;

    require("Storage").write("qalarm.json", JSON.stringify(alarms));
    eval(require("Storage").read("qalarmcheck.js"));
    showMainMenu();
  };
  if (!newAlarm) {
    menu["> " + L.delete] = function () {
      alarms.splice(timerIndex, 1);
      require("Storage").write("qalarm.json", JSON.stringify(alarms));
      eval(require("Storage").read("qalarmcheck.js"));
      showMainMenu();
    };
  }

  return E.showMenu(menu);
}

showMainMenu();
