/**
 * This file checks for upcoming alarms and schedules qalarm.js to deal with them and itself to continue doing these checks.
 */

print("Checking for alarms...");

if (Bangle.QALARM) {
  clearInterval(Bangle.QALARM);
  Bangle.QALARM = undefined;
}

function getCurrentTime() {
  let date = new Date();
  return (
    date.getHours() * 3600000 +
    date.getMinutes() * 60000 +
    date.getSeconds() * 1000
  );
}

let date = new Date();
let time = getCurrentTime();

let nextAlarms = (require("Storage").readJSON("qalarm.json", 1) || [])
  .filter(
    (alarm) =>
      alarm.on &&
      alarm.time > time &&
      alarm.last != date.getDate() &&
      (alarm.timer || alarm.daysOfWeek[date.getDay()])
  )
  .sort((a, b) => a.time - b.time);

if (nextAlarms[0]) {
  Bangle.QALARM = setTimeout(() => {
    eval(require("Storage").read("qalarmcheck.js"));
    load("qalarm.js");
  }, nextAlarms[0].time - time);
} else {
  // No alarms found: will re-check at midnight
  Bangle.QALARM = setTimeout(() => {
    eval(require("Storage").read("qalarmcheck.js"));
  }, 86400000 - time);
}
