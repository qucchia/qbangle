class QLocaleImplementation implements QLocale {
  name: string;
  translations: { [key: string]: string };
  decimalPoint: string;
  thousandsSep: string;
  currencySymbol: string;
  currencyFirst: boolean;
  intCurrSymbol: string;
  speed: string;
  distance: {
    0: string;
    1: string;
  };
  temperature: string;
  ampm: {
    0: string;
    1: string;
  };
  timePattern: {
    0: string;
    1: string;
  };
  datePattern: {
    0: string;
    1: string;
  };
  abMonth: [
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string
  ];
  month: [
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string
  ];
  abDay: [string, string, string, string, string, string, string];
  day: [string, string, string, string, string, string, string];

  constructor() {
    let name = require("Storage").readJSON("config.json").locale;
    if (!name) name = "en_GB.json";
    this.name = name;

    let locale = require("Storage").readJSON("locale_" + name + ".json");

    this.decimalPoint = locale.decimalPoint;
    this.thousandsSep = locale.thousandsSep;
    this.currencySymbol = locale.currencySymbol;
    this.currencyFirst = locale.currencyFirst;
    this.intCurrSymbol = locale.intCurrSymbol;
    this.speed = locale.speed;
    this.distance = locale.distance;
    this.temperature = locale.temperature;
    this.ampm = locale.ampm;
    this.timePattern = locale.timePattern;
    this.datePattern = locale.datePattern;
    this.abMonth = locale.abMonth;
    this.month = locale.month;
    this.abDay = locale.abDay;
    this.day = locale.day;
    this.translations = locale.translations;
  }

  formatNumber = (num: number, decimalPlaces = 2) => {
    let wholePart = num | 0;
    let decimalPart = Math.abs(num - wholePart);
    let decimals = (decimalPart.toFixed(decimalPlaces) + "").substring(2);

    let r = "";
    let str = "" + num.toFixed(decimalPlaces);
    let i = str.length;
    while (i > (num < 0 ? 1 : 0)) {
      r = this.thousandsSep + str.substring(i, i + 3) + r;
      i -= 3;
    }
    return (
      str.substring(0, i + 3) +
      r +
      (decimals ? this.decimalPoint + decimals : "")
    );
  };

  currency = (num: number) => {
    if (this.currencyFirst) {
      return this.currencySymbol + this.formatNumber(num);
    } else {
      return this.formatNumber(num) + this.currencySymbol;
    }
  };

  addLocale = (app: string) => {
    Object.assign(
      this.translations,
      require("Storage").readJSON(app + ".locale_" + this.name)
    );

    return this;
  };

  translate = (str: string) => {
    let translation = this.translations[str];
    if (translation) {
      return translation;
    } else {
      console.log("WARNING: Translation for", str, "not found.");
      return str;
    }
  };
}

exports = new QLocaleImplementation();
