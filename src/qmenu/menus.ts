import {
  Item,
  ItemMenu,
  ItemApp,
  ItemHandler,
  ItemSave,
  Input,
  ToggleInput,
  NumberInput,
} from "./items";
import { QMenuState } from "./qmenu";

let C = {
  HOR_MARGIN: 25,
  X: 25,
  Y: 50,
};

/** A menu. */
export class Menu {
  title: string;
  items: Item[] = [];
  state?: QMenuState;
  submenus: { [key: string]: Menu } = {};
  onGoBack?: () => void;
  onGoTo?: (url: string) => void;
  onSetMenuData?: (
    menuId: string,
    parameters: Object,
    menuData: Object
  ) => void;

  render = (): void => {}; // Specific to every menu type.

  constructor(title: string) {
    this.title = title;
  }

  addItem(item: Item) {
    item.menu = this;
    this.items.push(item);
  }

  addSubmenu(name: string, menu: Menu) {
    if (name in this.submenus) {
      console.error("addSubmenu: tried to add already existing submenu");
    }
    this.submenus[name] = menu;
  }

  selectItem(idx: number) {
    let item = this.items[idx];
    if (item) {
      item.select();
    } else {
      console.error("selectItem: item out of range");
    }
  }
}

/**
 * A menu with up to 3 items.
 * Each button on the BangleJS 1 corresponds to one item.
 * E.g, pressing BTN1 activates the 1st item.
 */
export class Menu123 extends Menu {
  constructor(title: string) {
    super(title);

    const BUTTONS = [BTN1, BTN2, BTN3];
    BUTTONS.forEach((button, i) => {
      setWatch(
        () => {
          this._handle123press(i + 1);
        },
        button,
        {
          repeat: true,
          edge: "rising",
        }
      );
    });
  }

  /** Renders a 123 menu. */
  override render = (): void => {
    /**
     * Renders a 123 menu item.
     * @param {number} itemNumber - The item's position (1 is the first item, 2 the second, etc.)
     * @param {string} text - The item's text
     * @param {number} y - The y position in px of the item box
     * @param {number} height - The height in px of the item box
     */
    function renderItem(
      itemNumber: number,
      text: string,
      y: number,
      height: number
    ) {
      g
        // Rectangle
        .drawRect(C.HOR_MARGIN, y, 240 - C.HOR_MARGIN, y + height)
        // Circle
        .setColor("#000")
        .fillCircle(C.HOR_MARGIN, y, 10)
        .setColor("#fff")
        .drawCircle(C.HOR_MARGIN, y, 10)
        .drawString(itemNumber, C.HOR_MARGIN + 2, y)
        // Text
        .drawString(text, 120, y + height / 2);
    }

    g.clear();
    g.reset();
    g.setFont("6x8", 2);
    g.setFontAlign(0, 0);
    g.drawString(this.title, 120, 20);
    let y = 50;

    this.items.forEach((item, i) => {
      renderItem(i + 1, item.title, y, 45);
      y += 65;
    });
  };

  /**
   * Handles the press of a button in a 123 menu.
   * @param {number} btn - The button number (1, 2 or 3) that was pressed.
   */
  _handle123press(btn: number) {
    this.selectItem(btn - 1);
  }
}

/** A menu which corresponds to Espruino's builtin menu. */
export class MenuUpDown extends Menu {
  /** Renders an UpDown menu. */
  override render = () => {
    /**
     * Renders an UpDown menu item.
     * @param {Item} item - The item to render.
     */
    const renderItem = (item: Item) => {
      if (
        item instanceof ItemMenu ||
        item instanceof ItemApp ||
        item instanceof ItemHandler
      ) {
        return () => {
          item.select();
        };
      } else if (item instanceof ItemSave) {
        return (): void => {
          if (this.state) {
            if (this.state.onSetMenuData) this.state.onSetMenuData();
            if (this.state.onGoBack) this.state.onGoBack();
          }
        };
      } else if (item instanceof Input) {
        if (!this.state || !this.state.menuData[item.id]) {
          throw new ReferenceError("input value not defined");
        }

        if (item instanceof ToggleInput) {
          if (typeof this.state.menuData[item.id] !== "boolean") {
            throw new TypeError("input value is not a boolean");
          }
          return {
            value: this.state.menuData[item.id],
            format: (value: boolean) => {
              if (!this.state) throw new ReferenceError("this.state is not defined");
              return this.state.locale.translate(
                value ? item.onLabel : item.offLabel
              );
            },
            onchange: (value: boolean) => {
              if (!this.state) throw new ReferenceError("this.state is not defined");
              this.state.menuData[item.id] = value;
            },
          };
        } else if (item instanceof NumberInput) {
          if (typeof this.state.menuData[item.id] !== "number") {
            throw new TypeError("input value is not a number");
          }
          return {
            value: this.state.menuData[item.id],
            step: item.step,
            min: item.min,
            max: item.max,
            wrap: item.wrap,
            onchange: (value: number) => {
              if (!this.state) throw new ReferenceError("this.state is not defined");
              this.state.menuData[item.id] = value;
            },
          };
        }
      }
      return undefined;
    };

    if (!this.state) throw new ReferenceError("this.state is not defined");

    let menu: { [key: string]: any };
    menu = { "": { title: this.state.locale.translate(this.title) } };

    if (this.onGoBack) {
      menu["< " + this.state.locale.translate("back")] = () => {
        if (this.onGoBack) this.onGoBack();
      };
    }

    for (const item of this.items) {
      menu[this.state.locale.translate(item.title)] = renderItem(item);
    }

    E.showMenu(menu);
  };
}
exports = { Menu, Menu123, MenuUpDown };
