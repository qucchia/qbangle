import { Menu } from "./menus";

/** A menu item. */
export class Item {
  readonly title: string;
  readonly label?: string;
  menu?: Menu;

  /** A function that is run when the item is selected. Specific to every menu item type. */
  select(): void {}

  /** Creates an Item.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} [label]: The item's label.
   */
  constructor(title: string, label?: string) {
    this.title = title;
    this.label = label;
  }
}

/** A menu item that opens another menu. */
export class ItemMenu extends Item {
  url: string;

  /** Creates an ItemMenu.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} url: The item's url.
   * @param {string} [label]: The item's label.
   */
  constructor(title: string, url: string, label?: string) {
    super(title, label);
    this.url = url;
  }

  /** Goes to the ItemMenu's menu. */
  override select(): void {
    if (!this.menu || !this.menu.onGoTo) {
      throw new ReferenceError("Did not find this.menu.onGoTo");
    }

    this.menu.onGoTo(this.url);
  }
}

/** A menu item that opens an app. */
export class ItemApp extends Item {
  appId: string;

  /** Creates an ItemApp.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} appId: The ID of the app to open.
   * @param {string} [label]: The item's label.
   */
  constructor(title: string, appId: string, label?: string) {
    super(title, label);
    this.appId = appId;
  }

  /** Loads the ItemApp's app. */
  override select() {
    load(this.appId + ".app.js");
  }
}

/** A menu item that opens a handler. */
export class ItemHandler extends Item {
  handlerId: string;

  /** Creates an ItemApp.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} handlerId: The ID of the handler to open.
   * @param {string} [label]: The item's label.
   */
  constructor(title: string, handlerId: string, label?: string) {
    super(title, label);
    this.handlerId = handlerId;
  }

  /** Loads the ItemHandler's handler. */
  override select() {
    let handler =
      require("Storage").readJSON("setting.json").handlers[this.handlerId];
    load(handler);
  }
}

/** A menu item that saves menu data. */
export class ItemSave extends Item {
  /** Saves the menu data. */
  override select() {
    if (!this.menu || !this.menu.state) {
      throw new ReferenceError("Did not find this.menu.state");
    }
    this.menu.state.onSetMenuData();
  }
}

/** A menu item that holds a value and can be changed by the user. */
export class Input extends Item {
  id: string;

  /** Creates an Input.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} id: The ID of the input.
   * @param {string} [label]: The item's label.
   */
  constructor(title: string, id: string, label?: string) {
    super(title, label);
    this.id = id;
  }
}

/** A menu input with a value that can be toggled. */
export class ToggleInput extends Input {
  onLabel: string;
  offLabel: string;

  /** Creates a ToggleInput.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} id: The ID of the input.
   * @param {string} onLabel: The item's label when its value is true.
   * @param {string} offLabel: The item's label when its value is false.
   */
  constructor(title: string, id: string, onLabel: string, offLabel: string) {
    super(title, id);
    this.onLabel = onLabel;
    this.offLabel = offLabel;
  }
}

/** A menu input with a value that can be toggled, with values "yes" for true and "no" for false. */
export class YesNoInput extends ToggleInput {
  /** Creates a YesNoInput.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} id: The ID of the input.
   */
  constructor(title: string, id: string) {
    super(title, id, "yes", "no");
  }
}

/** A menu input with a numerical value. */
export class NumberInput extends Input {
  step?: number;
  min?: number;
  max?: number;
  wrap?: boolean;

  /** Creates a NumberInput.
   * @constructor
   * @param {string} title: The item's title.
   * @param {string} id: The ID of the input.
   * @param {Object} [options]: Additional options.
   * @param {number} [options.step]: The input's step.
   * @param {number} [options.min]: The input's minimum value.
   * @param {number} [options.max]: The input's maximum value.
   * @param {boolean} [options.wrap]: Whether the number wraps around from its minimum to its maximum value and viceversa.
   */
  constructor(
    title: string,
    id: string,
    options?: {
      step?: number;
      min?: number;
      max?: number;
      wrap?: boolean;
    }
  ) {
    super(title, id);
    this.step = options?.step;
    this.min = options?.min;
    this.max = options?.max;
    this.wrap = options?.wrap;
  }
}

exports = {
  Item,
  ItemMenu,
  ItemApp,
  ItemHandler,
  ItemSave,
  Input,
  ToggleInput,
  YesNoInput,
  NumberInput,
};
