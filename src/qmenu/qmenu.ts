/*
 * QMenu is a menu framework which supports different types of menus and allows making
 * complex menus (with submenus and such) without having to worry about writing all
 * the code.
 */

// NOTE: The method of importing must be changed for use on the Bangle
import { Item, ItemApp, ItemHandler, ItemMenu } from "./items";
import { Menu, Menu123, MenuUpDown } from "./menus";

export class QMenuState {
  _menu: Menu;
  path: string;
  parameters: { [key: string]: any } = {};
  menuData: { [key: string]: any };
  locale: QLocale;
  previous?: QMenuState;
  onGoBack: () => void;
  onGoTo: (url: string) => void;
  onSetMenuData: () => void;

  constructor(
    menu: Menu,
    path: string,
    parameters: { [key: string]: any },
    menuData: { [key: string]: any },
    locale: QLocale,
    previous: QMenuState | undefined,
    goBack: () => void,
    goTo: (url: string) => void,
    setMenuData?: (
      menuId: string,
      parameters: { [key: string]: any },
      menuData: { [key: string]: any }
    ) => void
  ) {
    this._menu = menu;
    menu.state = this;
    this.path = path;
    this.parameters = parameters;
    this.locale = locale;
    this.menuData = menuData;
    this.previous = previous;
    this.onGoBack = goBack;
    this.onGoTo = goTo;
    if (setMenuData) {
      this.onSetMenuData = () => {
        setMenuData(this.path, this.parameters, this.menuData);
      };
    } else {
      this.onSetMenuData = () => {
	throw new ReferenceError("setMenuData is not defined");
      };
    }
  }

  render() {
    // Clear menu if there already is one
    E.showMenu();

    this._menu.render();
  }

  selectItem(idx: number) {
    this._menu.selectItem(idx);
  }
}

class QMenu {
  menus: { [key: string]: Menu } = {};
  locale: QLocale;
  state?: QMenuState;
  getMenuData?: (
    menuId: string,
    parameters: { [key: string]: any }
  ) => { [key: string]: any };
  setMenuData?: (
    menuId: string,
    parameters: { [key: string]: any },
    menuData: { [key: string]: any }
  ) => void;

  /**
   * Creates a QMenu object.
   * @constructor
   */
  constructor(app: string) {
    this.locale = require("qlocale").addLocale("qmenu").addLocale(app);
  }

  /** Starts the QMenu. */
  start() {
    this.goTo("");
  }

  /** Renders the current menu. */
  render() {
    if (!this.state) throw new ReferenceError("this.state is not defined");
    this.state.render();
  }

  /**
   * Selects a menu item.
   * @param {number} idx - The selected item.
   */
  selectItem(idx: number) {
    if (!this.state) throw new ReferenceError("this.state is not defined");
    this.state.selectItem(idx);
    /*
    if (item.type === "menu") {
      this.goTo(item.id);
    } else if (item.type === "app") {
      load(item.id + ".app.js");
    } else if (item.type === "handler") {
      let handler =
        require("Storage").readJSON("setting.json").handlers[item.id];
      load(handler);
    } else if (item.type === "back") {
      this.goBack();
    } else if (item.type === "action") {
      let itemArguments;
      if (Array.isArray(item.arguments)) {
        itemArguments = item.arguments;
      } else {
        itemArguments = [item.arguments];
        this[item.action](item.arguments);
      }
      this[item.action].apply(this, item.arguments.concat(parameters));
    }
    */
  }

  /**
   * Sets the path, updates and renders the menu.
   * @param {string} url - The path to set.
   */
  goTo(url: string) {
    const array = url.split("?");
    const path = array[0];
    if (!path) throw new Error("no path provided");

    const parameterString = array[1];

    let parameters: { [key: string]: string } = {};
    if (parameterString) {
      parameterString.split("&").forEach((parameter) => {
        let array2 = parameter.split("=");
        let key = array2[0];
        if (!key) return;

        let value = array2[1];
        if (value === undefined) value = key;

        parameters[key] = value;
      });
    }

    let menuData = {};
    if (this.getMenuData) menuData = this.getMenuData(path, parameters);

    const menu = this.menus[path];

    if (!menu) {
      E.showAlert(this.locale.translate("not_found")).then(() => {
        this.goBack();
      });
      return;
    }

    this.state = new QMenuState(
      menu,
      path,
      parameters,
      menuData,
      this.locale,
      this.state,
      this.goBack,
      this.goTo,
      this.setMenuData
    );
    this.render();
  }

  /** Goes to the previous menu. */
  goBack() {
    if (!this.state) {
      load();
    } else {
      this.state = this.state.previous;
    }
    this.render();
  }
}

exports = {
  QMenu,
  QMenuState,
  Item,
  ItemMenu,
  ItemApp,
  ItemHandler,
  Menu,
  Menu123,
  MenuUpDown,
};
