/*
 * Qmenu is a menu framework which supports different types of menus and allows making
 * complex menus (with submenus and such) without having to worry about writing all
 * the code.
 */

let C = {
  HOR_MARGIN: 25,
  X: 25,
  Y: 50,
};

exports = class Qmenu {
  /**
   * Creates a Qmenu object.
   * @constructor
   * @param {(string|Object)} src - Either the name of the app to load or the object.
   */
  constructor(L, menus) {
    this.menus = menus;
    this.L = L;

    this.history = [];
    this.state = {};

    setWatch(
      () => {
        if (this.menu.type === "123") {
          this.handle123press(1);
        }
      },
      BTN1,
      {
        repeat: true,
        edge: "rising",
      }
    );

    setWatch(
      () => {
        if (this.menu.type === "123") {
          this.handle123press(2);
        }
      },
      BTN2,
      {
        repeat: true,
        edge: "rising",
      }
    );

    setWatch(
      () => {
        if (this.menu.type === "123") {
          this.handle123press(3);
        }
      },
      BTN3,
      {
        repeat: true,
        edge: "rising",
      }
    );
  }

  getDynamicMenu() {}
  getMenuData() {}
  setMenuData() {}

  /** Starts the Qmenu. */
  start() {
    this.setPath("");
  }

  /** Renders the current menu. */
  render() {
    // Clear menu if there already is one
    E.showMenu();

    if (this.menu.type === "123") {
      render123Menu();
    } else if (this.menu.type === "UpDown") {
      renderUpDownMenu();
    }
  }

  /** Renders a 123 menu, which has up to 3 items. */
  render123Menu() {
    /**
     * Renders a 123 menu item.
     * @param {number} number - The item's position (1 is the first item, 2 the second, etc.)
     * @param {string} text - The item's text
     * @param {number} y - The y position in px of the item box
     * @param {number} height - The height in px of the item box
     */
    function renderItem(itemNumber, text, y, height) {
      g
        // Rectangle
        .drawRect(C.HOR_MARGIN, y, 240 - C.HOR_MARGIN, y + height)
        // Circle
        .setColor("#000")
        .fillCircle(C.HOR_MARGIN, y, 10)
        .setColor("#fff")
        .drawCircle(C.HOR_MARGIN, y, 10)
        .drawString(itemNumber, C.HOR_MARGIN + 2, y)
        // Text
        .drawString(text, 120, y + height / 2);
    }

    g.clear();
    g.reset();
    g.setFont("6x8", 2);
    g.setFontAlign(0, 0);
    g.drawString(this.L[this.menu.title], 120, 20);
    let y = 50;
    for (let i = 0; i < this.menu.items.length; i++) {
      renderItem(i + 1, this.menu.items[i].title, y, 45);
      y += 65;
    }
  }

  /** Renders an UpDown menu, which corresponds to Espruino's builtin menu. */
  renderUpDownMenu() {
    /**
     * Renders an UpDown menu item.
     * @param {Object} item - The item to render.
     */
    function renderItem(item) {
      if (item.type === "menu" || item.type === "app") {
        return () => {
          this.selectItem(item);
        };
      } else if (item.type === "save") {
        return () => {
          this.setMenuData(this.menuData);
          this.goBack();
        };
      } else if (item.type === "bool") {
        return {
          value: this.menuData[item.id],
          format: (value) =>
            value ? this.L[item.onLabel] : this.L[item.offLabel],
          onchange: (value) => (this.menuData[item.id] = value),
        };
      } else if (item.type === "yesNo") {
        return {
          value: this.menuData[item.id],
          format: (value) => (value ? this.L.yes : this.L.no),
          onchange: (value) => (this.menuData[item.id] = value),
        };
      } else if (item.type === "number") {
        return {
          value: this.menuData[item.id],
          step: item.step,
          min: item.min,
          max: item.max,
          wrap: item.wrap,
          onchange: (value) => {
            this.menuData[item.id] = value;
          },
        };
      }
    }

    let menu = { "": { title: this.L[this.menu.title] } };

    if (this.history.length > 1) {
      menu["< " + this.L.back] = () => this.goBack();
    }

    this.menu.items.forEach((item) => {
      menu[this.L[item.title]] = renderItem(item);
    });

    E.showMenu(menu);
  }

  /**
   * Selects a menu item.
   * @param {Object} item - The selected item.
   */
  selectItem(item) {
    if (item.type === "menu") {
      this.setPath(item.id);
    } else if (item.type === "app") {
      load(item.id + ".app.js");
    } else if (item.type === "handler") {
      let handler =
        require("Storage").readJSON("setting.json").handlers[item.id];
      load(handler);
    } else if (item.type === "back") {
      this.goBack();
    } else if (item.type === "action") {
      let itemArguments;
      if (Array.isArray(item.arguments)) {
        itemArguments = item.arguments;
      } else {
        itemArguments = [item.arguments];
        this[item.action](item.arguments);
      }
      this[item.action].apply(this, item.arguments.concat(parameters));
    }
  }

  /**
   * Sets the path, updates and renders the menu.
   * @param {string} path - The path to set.
   * @param {boolean} [dontAddToHistory=false] - If true, don't add path to history.
   */
  setPath(path, dontAddToHistory) {
    if (!dontAddToHistory) {
      this.history.push(path);
    }

    const array = path.split("?");
    this.path = array[0];
    let parameterString = array[1];

    let parameters = {};
    if (parameterString) {
      parameterString.split("&").forEach((parameter) => {
        let [key, value] = parameter.split("=");
        paramaters[key] = vale;
      });
    }

    this.menuData = this.getMenuData(menuId, parameters);

    this.menu = require("Storage").readJSON(this.src)[this.path];
    if (this.menu === "dynamic") {
      this.menu = this.getDynamicMenu(path);
    }
    if (typeof this.menu !== "object") {
      E.showAlert("Oh no! The menu you were looking for was not found.").then(
        () => {
          this.goBack();
        }
      );
      return;
    }

    this.render();
  }

  /** Goes to the previous location in history. */
  goBack() {
    this.history.pop();
    this.setPath(this.history[this.history.length - 1], true);
  }

  /**
   * Handles the press of a button in a 123 menu.
   * @param {number} btn - The button number (1, 2 or 3) that was pressed.
   */
  handle123press(btn) {
    let item = this.menu.items[btn - 1];
    this.selectItem(item);
  }
};
