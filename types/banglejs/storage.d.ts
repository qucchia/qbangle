declare interface Storage {
  compact: () => void;
  erase: (name: string) => void;
  eraseAll: () => void;
  getFree: () => number;
  hash: (regex?: RegExp) => number; // More specifically it returns Uint32
  list: (regex?: RegExp, filter?: { sf: boolean }) => string[];
  // TODO: Describe return type of open()
  open: (name: string, mode: "r" | "w" | "a") => object;
  read: (name: string, offset?: number, length?: number) => string | undefined;
  readArrayBuffer: (name: string) => ArrayBuffer | undefined;
  readJSON: (name: string, noExceptions?: boolean) => any;
  write: (
    name: string,
    data: string | Array<any> | Object,
    offset?: number,
    size?: number
  ) => boolean;
  writeJSON: (name: string, data: any) => boolean;
}
