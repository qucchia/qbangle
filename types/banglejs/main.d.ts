/*~ These are the type declarations for Espruino on the Bangle.JS. */

/// <reference path="globals.d.ts" />
/// <reference path="espruino.d.ts" />
/// <reference path="graphics.d.ts" />
/// <reference path="i2c.d.ts" />
/// <reference path="serial.d.ts" />
/// <reference path="storage.d.ts" />

/// <reference path="qlocale.d.ts" />

declare const Graphics: GraphicsApi;
declare const g: GraphicsApi;

type Widget = {
  area: "tr" | "tl";
  width: number;
  draw: (this: { x: number; y: number }) => void;
};

declare const WIDGETS: { [key: string]: Widget };

declare let exports: any;
