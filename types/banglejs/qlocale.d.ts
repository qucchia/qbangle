/*~ This file declares the QLocale library from this project.
 *~ It's located at /src/qlocale/qlocale.ts
 */

type QLocale = {
  name: string;
  translations: { [key: string]: string };
  decimalPoint: string;
  thousandsSep: string;
  currencySymbol: string;
  currencyFirst: boolean;
  intCurrSymbol: string;
  speed: string;
  distance: {
    0: string;
    1: string;
  };
  temperature: string;
  ampm: {
    0: string;
    1: string;
  };
  timePattern: {
    0: string;
    1: string;
  };
  datePattern: {
    0: string;
    1: string;
  };
  abMonth: [
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string
  ];
  month: [
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string,
    string
  ];
  abDay: [string, string, string, string, string, string, string];
  day: [string, string, string, string, string, string, string];

  formatNumber: (num: number, decimalPlaces: number) => string;

  currency: (num: number) => string;

  addLocale: (app: string) => QLocale;

  translate: (str: string) => string;
}
